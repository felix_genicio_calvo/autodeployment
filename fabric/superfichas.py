#!/usr/bin/env python
# -*- coding: utf-8 -*-

from imports import *
import httpd
import users
import git
import fabfile
import composer

# Aqui definimos las funciones especificas del proyecto que vamos a desplegar, en principio este es el unico fichero que deberiamos editar
# Para hacer un despliegue de otro proyecto, podemos usar este como ejemplo, copiandolo y pegandolo, y editando los ficheros
# .fabricrc y conf/web0xconf.py

def new_sf_provision( packages=conf.packages ):
    fabfile.provision(packages)

def new_sf_workspace( username, vhostname, path, repo=conf.repo, ssh_key='', project_name=conf.project_name ):
    fabfile.deploy_user( username, path, vhostname, repo, keyfile=ssh_key )
    new_sf_site( vhostname, path, repo, branch=conf.branch, project_name=project_name ) 

def new_sf_site( vhostname, path, repo=conf.repo, branch=conf.branch, project_name=conf.project_name ):
    fabfile.deploy_site( vhostname, path, repo, branch )
    new_sf_config(path, vhostname, project_name)

## Con este método desplegamos la configuración específica de un projecto, en este caso superfichas.
def new_sf_config(path, vhostname, project_name=conf.project_name):
    #Subimos el composer.json de superfichas
    put(conf.composer_example_file, path + '/composer.json', use_sudo=True)

    # Instalamos composer.phar en el path
    composer.setup(path)
    # Actualizamos composer
    composer.update(path)
    # Subimos el local.php... 
    put(conf.web_config_file, path + '/etc/local.php', use_sudo=True)
    conf.project_vars = {
        '{{path}}':path,
        '{{project_name}}':'superfichas',
        '{{domain}}':'santi.superfichas.com'
    }
    
    fabfile.replace_variables(conf.project_vars, path + '/etc/local.php')

    mode_sudo()
    dir_ensure(path + "/var/log/", True)

    #TODO Instalar extjs4

    mode_user()


def update(repo,path):
    dir_ensure(path, True)
    git.pull(repo, path, True)

def checkout_branch(repo,path,branch):
    dir_ensure(path, True)
    git.checkout_branch(repo, path, branch )

def checkout_tag(repo,path,branch):
    dir_ensure(path, True)
    git.checkout_tag(repo, path, tag )

