# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Project   : superfichas - AutoDeploy de servidores y vhosts usando fabric & cuisine
# -----------------------------------------------------------------------------
# License   : 
# -----------------------------------------------------------------------------
# Authors   : Antonio Jerez                              <pobreiluso@gmail.com>
# -----------------------------------------------------------------------------
# Creation  : 13-Dic-2014
# Last mod  : 18-Mar-2015
# -----------------------------------------------------------------------------


"""
Con estos scripts, automatizamos todas las tareas de despliegue de superfichas.com

Estructura de directorios:
    - .ssh/
     |   id_dsa, id_dsa.pub  ## Clave pública y privada autorizada en servidores
    - conf/ 
      |___ sp0x/             ## Spaghetti 1.1, ubuntu 14 config files.
        |___ apache2/           ## Apache2 complete production config
        |___ assets/            ## img folder
        |___ beanstalkd/        ## 
        |___ collectd/          ## Monitorización a graphite.
        |___ cron.d/            ## prestar atención a cron.d/superfichas
        |___ munin/
        |___ nagios3/
        |___ php5/              
        |___ redis/             ## Redis para cache y sessiones php  centraliadas en sp01
        |___ supervisor/        ## transactionalWorker.conf
        |___ apt-packages.txt   ## Listado de paquetes de dpkg-cache sacado con ../scripts/installedpkgs
        |___ fstab
        |___ get-selections.txt ## resultado de ejecutar dpkg-get-selections > get-selections.txt
        |___ src/               ## Contiene los ficheros de configuración locales de cada proyecto, no disponibles en el repositorio

      |___ web0x/            ## Laravel 2.0, ubuntu 14
        |___ nginx/
        |___ fpm/
        |___ apt-packages.txt

    - fabric/
        |___ apache2.py      ## Apache related functions
        |___ composer.py     ## composer ..
        |___ conf.py         ## En este fichero, cambiamos a mano el fichero de config segun el tipo 
                             ## de despliegue que queramos hacer web0x laravel, sp0x spaghetti
        |___ deploy.py       ## deploy de ramas y tags a entornos.
        |___ fabfile.py      ## Fichero principal del despliegue, contiene funciones genericas
        |___ fpm.py          ## Apache
        |___ git.py          ## Git
        |___ graphitefabfile.py  
        |___ imports.py      ## Fichero con includes generales
        |___ laravel.py      ## Funciones para desplegar laravel.
        |___ nagiosfabfile.py ## Nagios
        |___ nginx.py        ## nginx
        |___ ovhtest.py      ## Test de interacción con al API de Ovh
        |___ sp0xconf.py     ## configuraciones del stack, servidores nfs, entradas fstab, symlinks ...
        |___ web0xconf.py    ## configuraciones del stack, servidores nfs, entradas fstab, symlinks ...
        |___ spaghetti.py    ## Funciones para desplegar laravel.
 
    - scripts/
        |___ blueprint.sh    ## Historail de paquetes instalados para la ISO
        |___ installedpkgs.sh 

"""

## Hacemos los imports generales.
from imports import *
from superfichas import *
from git import *
## Importamos la configuración vigente *** Importante, cambiar a mano en conf.py
#from conf import *


# fab provision:ajerez.superfichas.com, /var/www/vhosts/ajerez.superfichas.com/, 172.16.0.40, spf-src.git,

#def provision(vhostname, path, vip, repo=conf.repo, fpmpool=conf.fpmpool, example_file=conf.web_example_file):

def provision(packages=conf.packages):
    install_packages(packages)
    setup_packages()
    restart_services()

def deploy_user(username, path, vhostname, repo=conf.repo, keyfile=False):
    users.useradd(username, sudoers=False, keyfile=keyfile)
    #deploy_site(vhostname,path,repo)

def deploy_site(vhostname, path, repo=conf.repo, branch=conf.branch):
    install(repo,path)
    httpd.create_site(vhostname, path)

def setup_packages():
    httpd.setup()
    #mysql_setup()
    #setup_limits()

def install(repo,path):
    dir_ensure(path, True)
    git.clone(repo, path, True)

# =============================================================================
#
# Uno de los pasos más importantes a la hora de hacer un setup de un nuevo
# servidor, "Raise Limits"
#
# =============================================================================


def setup_limits():
    sysctlfilename = "/etc/sysctl.conf"
    sysctlfile = file_read(sysctlfilename)

    sysctlfile = text_ensure_line(sysctlfile, 'net.ipv4.netfilter.ip_conntrack_max = 131072')
    sysctlfile = text_ensure_line(sysctlfile, 'net.ipv4.tcp_syncookies=1')
    sysctlfile = text_ensure_line(sysctlfile, 'net.core.somaxconn=250000')
    sysctlfile = text_ensure_line(sysctlfile, 'net.ipv4.tcp_max_syn_backlog=2500"')
    sysctlfile = text_ensure_line(sysctlfile, 'net.core.netdev_max_backlog=2500')
    sysctlfile = text_ensure_line(sysctlfile, 'net.nf_conntrack_max=1000000')

    file_update(sysctlfilename, lambda _:sysctlfile)

    run ("sysctl -p > /dev/null")

    limitsfilename = "/etc/security/limits.conf"
    limitsfile = file_read(limitsfilename)

    limitsfile = text_ensure_line(limitsfile, "* hard nofile 64000")
    limitsfile = text_ensure_line(limitsfile, "* soft nofile 64000")
    limitsfile = text_ensure_line(limitsfile, "root hard nofile 64100")
    limitsfile = text_ensure_line(limitsfile, "root soft nofile 64100")

    file_update(limitsfilename, lambda _:limitsfile)

    pam_limits_filename = "/etc/pam.d/common-session"
    pam_limits = file_read(pam_limits_filename)
    pam_limits = text_ensure_line(pam_limits, "nsession required pam_limits.so")


    file_update(pam_limits_filename, lambda _:pam_limits)


# =============================================================================
#
# Usando la funcion package_ensure de cuisine, nos aseguramos de que todos los
# paquetes necesraios esten disponibles. Por defecto, lee el valor de conf.packages
# que a su vez, será web0xconf.packages o sp0xconf.packages en función de como 
# se haya definido en conf.py
#
# =============================================================================

def install_packages(pkgList=conf.packages):
    #run("sudo add-apt-repository ppa:ondrej/php5-5.6")
    #run("sudo apt-get update")
    select_package("yum")
    package_ensure(pkgList)

# =============================================================================
#
# Instala el contenido del fichero conf.dpkg-selections.txt
#
# =============================================================================
#def set_selections(selections=conf.dpkgselections):
#    put(selections, "/root/dpkg-selections.txt")
#    run("sudo dpkg --set-selections < /root/dpkg-selections.txt")


def restart_services():
    #upstart_restart('php5-fpm')
    upstart_restart('httpd')


# =============================================================================
#
# Funcion generica de templating, recibe un fichero, un caracter separador, y 
# una lista de variables, y busca la ocurrencia de las variables para actualizar
# su valor. 
#
# =============================================================================
def update_configuration(config_file, separator, variables):
    configuration = file_read(config_file)
    for key, value in variables.items():
        configuration, replaced = text_replace_line(
            configuration,
            key + separator,
            key + separator + str(value),
            process=lambda text:text.split(separator)[0].strip()
        )
        configuration = text_ensure_line(configuration, key + separator + str(value))

    return configuration

def replace_variables(variables, config_file_path):
    for key, value in variables.items():
        fabric.contrib.files.sed(config_file_path, re.escape(key), str(value), use_sudo=True)

# =============================================================================
#
# Esta funcion esta diseñada para ser ejecutada contra el servidor NFS 
# (IP disponible en conf.py), se asegura de que exista un recurso compartido, 
# caso de no existir lo añade y exporta el nuevo recurso. La usamos para montar 
# directorios compartidos de etiquetas, temporales, imagenes, etc etc
#
# =============================================================================
def exportfs_add(host, directory):
    exports_file = "/etc/exports"

    #return env.host_string

    exports = file_read(exports_file)
    line = False
    line = directory + " " + host +"(rw,sync,no_subtree_check,no_root_squash)"
    exports = text_ensure_line(exports, line)

    file_update(exports_file, lambda _:exports)

    run("exportfs -a")

# =============================================================================
#
# Creamos, autorizamos, añadimos al fstab y montamos los directorios compartidos.
#
# =============================================================================
def mount_nfs():
    fstab_file = "/etc/fstab"
    server_ip = socket.gethostbyname(env.host)

    for remote, path in nfs_dirs.items():
        dir_ensure(path, True)
        dir_attribs(path, mode=775, owner='www-data', group='www-data', recursive=False)

    fstab = file_read(fstab_file)
    for key, value in fstab_lines.items():
        fstab = text_ensure_line(fstab, value)
    file_update(fstab_file, lambda _: fstab)

    ## A continuacion añadimos al servidor NFS las carpetas compartidas que correspondan
    with settings(
        hide('warnings', 'running', 'stdout', 'stderr'),
        warn_only=True
    ):
        for nfs_dir, mount_dir in nfs_dirs.items():
            for name, host in nfs_servers.items():
                with settings(host_string=host, i=".ssh/id_dsa"):
                    exportfs_add(server_ip, nfs_dir)
            #result = run("umount -l -f " + mount_dir + " 2>/dev/null")
            #result = run("mount " + mount_dir + "2>/dev/null")
def nagios_install():
    NagiosFullInstall()
