from imports import *
import cuisine
import fabfile
import conf

def set_key():
    sshdir = "/root/deploy/.ssh/"
    sshkey = "id_dsa"
    sshconfigfile = "/root/.ssh/config"

    dir_ensure(sshdir, True)
    file_ensure(sshconfigfile)
    put( "../.ssh/id_dsa", sshdir)
    file_attribs(sshdir + sshkey, "600")

    sshvariables = {
                'Hostname':'git.bitbucket.com',
                'IdentityFile':'/root/deploy/.ssh/id_dsa',
            }

    file_update(sshconfigfile, lambda _ : fabfile.update_configuration(sshconfigfile, " ", sshvariables))

def del_key():
    dir_remove("/root/deploy/")
    #file_delete("/root/.ssh/config") # Ojo, borramos la configuracion de ssh de root, modificar por un borrar lineas si usamos configuracion personalizada en ssh
    #run( "ssh-agent (ssh-add .ssh/id_dsa; git clone git@github.com:%s %s)" % (repo, path))

def clone(repo, path, reset):
    mode_sudo()
    #if reset:
    dir_remove(path, True)
    dir_ensure(path)
    dir_attribs(path, mode=775, owner=conf.default_user, group=conf.default_group, recursive=True)
    #set_key()
    mode_user()
    run( "git clone %s/%s %s" % (conf.repo_url, repo, path+'/') )
    #del_key()

    mode_sudo()
    dir_attribs(path, mode=775, owner=conf.default_user, group=conf.default_group, recursive=True)

    mode_user()

def pull(repo, path, branch):

    newbranch = False
    if branch:
        newbranch=branch
    dir_ensure(path)
    set_key()
    with cd(path):
        run( "git checkout . -f" )
        run( "git pull origin " + branch)

    del_key()
    #run( "ssh-agent (ssh-add .ssh/id_dsa; git clone git@github.com:%s %s)" % (repo, path))
    dir_attribs(path, mode=775, owner=conf.default_user, group=conf.default_group, recursive=True)

def reset_hard(repo,path,branch):
    set_key()
    with cd(path):
        run( "git fetch --all" )
        run( "git checkout origin/" + branch )
        run( "git reset --hard origin/" + branch )
        run( "git checkout -f . " )
        #run( "git pull origin " + branch)

    del_key()

def checkout_tag(repo,path,tag):
    dir_ensure(path)
    set_key()
    with cd(path):
        run( "git fetch --all ")
        run( "git checkout . -f" )
        if tag:
            run( "git checkout " + tag )
        else:
            run( "git pull origin master")

    del_key()
    dir_attribs(path, mode=775, owner=conf.default_user, group=conf.default_group, recursive=True)

def checkout_branch(repo,path,branch):
    mode_sudo()
    dir_ensure(path)
    #set_key()
    with cd(path):
        mode_user()
        run( "git fetch --all ")
        run( "git checkout . -f" )
        if branch:
            run( "git checkout " + branch )
        else:
            run( "git pull origin master")

    #del_key()
    mode_sudo()
    dir_attribs(path, mode=775, owner=conf.default_user, group=conf.default_group, recursive=True)