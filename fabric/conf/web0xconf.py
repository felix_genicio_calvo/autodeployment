packagesFile = './conf/web0x/centos-packages.txt';
packages = [line.strip() for line in open(packagesFile)]

packagesFile = '';

database_dump = "https://www.dropbox.com/s/3vkbnstvcmtsj0f/conjunto_datos.zip"

web_example_file = "./conf/web0x/httpd/sites-available/web.superfichas.com"
api_example_file = "./conf/web0x/httpd/sites-available/api.superfichas.com"
web_config_file = "./conf/web0x/src/local.php"
composer_example_file = "./conf/web0x/composer/composer.json"
repo = 'cdr-sf.git'
project_name = 'superfichas'
## Not Used With Apache
fpmpool      = 'www'
branch = "master"
tag = False

repo_url = "git@bitbucket.org:ajerezguillen"

default_user = "centos"
default_group = "developers"

http_conf_variables = {}
http_conf_patterns = {
    '{{root}}':'',
    '{{servername}}':'',
    '{{fpm_socket}}':''
}

project_vars = {
    "{{path}}":'',
    "{{project_name}}":'superfichas',
    "{{domain}}":''
}

nfs_servers = {
    #'static01':'37.187.79.42',
}

nfs_dirs = {
        "/var/www/static.superfichas.com/pictures":"/var/www/static.superfichas.com/pictures",
        "/var/www/static.superfichas.com/tmp":"/var/www/static.superfichas.com/tmp",
        #"/var/www/static.superfichas.com/archivos":"/var/www/static.superfichas.com/archivos"
}

fstab_lines = {
    'pictures':'5.196.78.12:/var/www/static.superfichas.com/pictures /var/www/static.superfichas.com/pictures  nfs        noatime,relatime,rw,hard,intr,timeo=5,retrans=5,actimeo=10,retry=5,bg,soft,intr,async,vers=3 0 0',
    'descargas':'5.196.78.12:/var/www/static.superfichas.com/descargas    /var/www/static.superfichas.com/descargas  nfs noatime,relatime,rw,hard,intr,timeo=5,retrans=5,actimeo=10,retry=5,bg,soft,intr,async,vers=3 0 0',
    'tmp':'5.196.78.12:/var/www/static.superfichas.com/tmp /var/www/static.superfichas.com/tmp  nfs        noatime,relatime,rw,hard,intr,timeo=5,retrans=5,actimeo=10,retry=5,bg,soft,intr,async,vers=3 0 0'
}

#nagios_server_public_ip = '5.196.78.12' # web02.fra.gr1.superfichas.com
#nagios_server_private_ip = '172.16.0.1' # web02.fra.gr1.superfichas.com

#nginx_conf = {'conf','patterns'}
#fpm_conf   = {'conf','patterns'}

fpm_conf_variables = {
    'listen':'/var/run/php-fpm.sock',
    'pm':'ondemand',
    'pm.max_children':1024,
    'pm.status_path':'/status',
    'php_admin_flag[log_errors]': 'on',
    #'php_admin_flag[error_log]': '/var/log/fpm/php' + fpmpool + '.log',
    'php_admin_value[memory_limit]': '2256M',
    'php_admin_value[error_reporting]': 'E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE & ~E_WARNING;',
    'php_admin_value[upload_tmp_dir]': '/var/www/static.superfichas.com/tmp/',
}


fpm_conf_patterns = {
    '{{poolname}}':'www'
}