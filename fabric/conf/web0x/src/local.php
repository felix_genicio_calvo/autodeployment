<?php

// Definiciones locales van aqui
error_reporting(E_ERROR | E_PARSE);

$root_path = "{{path}}";
$project_name = "{{project_name}}";
$domain = "{{domain}}";

$sf_paths[] = $root_path."/vendor/zendframework/zendframework1/library";
$sf_paths[] = $root_path."/vendor/apache/log4php/src/main/php";
$sf_paths[] = $root_path."/vendor/phpoffice/phpexcel/Classes/PHPExcel";
$sf_paths[] = $root_path."/vendor/phpunit/phpunit/src";


// Path to PHPUnit
$sf_paths[] = "/usr/share/php";
// ------------------------------
$sf_cfg['db-dns'] = "127.0.0.1";


// Conexión por defecto a superfichas
$sf_cfg["db_host"] = $sf_cfg['db-dns'];
$sf_cfg["db_username"] = "superfichas";
$sf_cfg["db_password"] = "xxxxxxxxxxx";

$sf_cfg["compatiblidad_hacia_atras"] = 1;
$sf_cfg["activo_fase_1"] = 1;

$sf_cfg["activo_fase_2"] = 1;
$sf_cfg["fotos_no_migrar"] = true;

$sf_cfg["db_profiler"] = true;

$sf_cfg["suggest_url"] = "http://".$domain."/req_suggest.cgi";

$sf_cfg["sistema_reservas_url"] = "http://".$domain."/sssss";

$sf_cfg["sistema_reservas_gen_ficheritos"] = "http://".$domain."/export.cgi";

$sf_cfg["sistema_localidades_db"] = "localidades_des";
$sf_cfg["sistema_nges_db"] = "nges_des";

$sf_cfg["sistema_cdrcom_db"] = "cdrcom_des";
$sf_cfg["sistema_cdrcom_url"] = "http://".$domain;

$sf_cfg["php_display_errors"] = 1;

$sf_cfg["sistema_gestion_db"] = "gestion_des";
$sf_cfg["sistema_suggest_db"] = "gestion_des";

$sf_cfg["extjs_url"] = "../extjs-4.0.7-src";
//$sf_cfg["extjs_url"] = "/ext-4.2.1";
$sf_cfg["extjs_boot"] = "ext-all-debug-w-comments.js";
$sf_cfg["extjs_blank_image_url"] = "";

$sf_cfg["ips_privilegiadas"] = "*";

$sf_cfg["deploy_server_prod_user"] = "www";
$sf_cfg["deploy_server_prod_location"] = "";

$sf_cfg["deploy_server_preprod_user"] = "www";
$sf_cfg["deploy_server_preprod_location"] = "";

$sf_cfg["sistema_mig_gestion_db"] = "superfichas";

$sf_cfg["sistema_cdrcom_db"] = "cdrcom_des";
$sf_cfg["sistema_cdrcom_url"] = "http://".$domain;

$sf_cfg["deploy_server_prod_user"] = "www";
$sf_cfg["deploy_server_prod_location"] = "";

$sf_cfg["deploy_server_preprod_user"] = "www";
$sf_cfg["deploy_server_preprod_location"] = "";

$sf_cfg['sistema_superfichas_url'] = 'http://'.$domain.com'/trunk/public';

#$sf_cfg["login_default_user"] = 'javier';
#$sf_cfg["login_default_passwd"] = 'pass';

$sf_cfg["www_base"] = "http://";
$sf_cfg["ws_nges"] = "http://";
$sf_cfg["mayoristas_no_actualizar"] = array('HBED');

// CONFIG FOTOS2
$sf_cfg["icomparer_url"]    = "";
//$sf_cfg["icomparer_type"]    = "LOCAL";
$sf_cfg["fotos_tmp_icomparer"] = '/tmp';
$sf_cfg["fotos2_base_url"] = 'http://sf.local/public';
$sf_cfg["fotos2_base_dir"] = '/var/www/html/fotos';
$sf_cfg["safety_file"] = 'README-local.txt';
$sf_cfg["foto2_migracion"] = true;
$sf_cfg["icomparer_iterativo"] = false;
$sf_cfg["mayorista_forzar_normalizacion"] = true;
$sf_cfg["foto_calidad_minima"] = 0;
$sf_cfg["fotos_minimo_api"] = 300;
$sf_cfg["fotos_ruta"] = 'fotos_prod';

$sf_cfg["fotos_write_mode"] = 'on';

$sf_cfg["booking_guardar_urls"] = true;

// CACHÉ
$sf_cfg["cache_activa"]     = false;
$sf_cfg["cache_fontend"]    = 'Core';
$sf_cfg["cache_backend"]    = 'File';
$sf_cfg["cache_dir"]        = '../var/cache';
$sf_cfg["cache_lifetime"]   = 60 * 60 * 24;
$sf_cfg["cache_niveles"]    = 3;


// Multiples conexiones a bases de datos
$sf_cfg['db_superfichas'] = array(
    'host'=>$sf_cfg['db-dns'],
    'port'=>"3306",
    "username"=>$sf_cfg['db_username'],
    "password"=>$sf_cfg['db_password'],
    "schema"=>'superfichas',
    "profiler"=>false,
    "pool_max"=>5
);
// El usuario superfichas sólo debería tener acceso de escritura a las tablas localidades_geoposicion y localidades_mapeo_mirror
$sf_cfg['db_localidades'] = array(
    'host'=>$sf_cfg['db-dns'],
    'port'=>"3306",
    "username"=>$sf_cfg['db_username'],
    "password"=>$sf_cfg['db_password'],
    "schema"=>$sf_cfg['sistema_localidades_db'],
    "profiler"=>false,
    "pool_max"=>5
);
