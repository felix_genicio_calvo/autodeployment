# -*- coding: utf-8 -*-
from fabric.api import *
from fabric.context_managers import settings
from fabric.network import ssh
from cuisine import *
import os
import conf

env.user = conf.default_user
# Example:
# fab -H 5.196.79.31 useradd:javialgaba,False,chicfy-laravel.git,javivel.chicfy.com/
def useradd(username, sudoers=False, keyfile=False):
    #env.user  = 'root'
    group     = 'developers'
    home_path = '/home/'
    #keys_path = '/root/fabric/keys/'
    default_shell = '/bin/bash';

    if not ( group_check( group ) ) :
        group_create( group )
    else:
        puts("Group Alredy Exists")

    if not ( user_check( username ) ) :
        user_create( username, None, '{home}{username}'.format(home=home_path, username=username), shell='{shell}'.format(shell=default_shell), createhome=True)
    else:
        puts("User Already exists")

    # Add user to group, and make it the default group for that user
    if not ( group_user_check(group, username)  ) :
        set_user_primary_group(group, username)
        group_user_add(group, username)

    if sudoers == True :
	    add_sudoers(username)

    #with settings(user=username):
    with mode_sudo():
        ssh_keygen( username )

    authorize_key( username, keyfile )

def authorize_local_key( username_from, username_to ) :
    ##env.user = username_from
    run('echo "%(user)s"' % env)
    key_file_local = open(('/home/%s/.ssh/id_dsa.pub') % (username_from), "r")
    key_local = key_file_local.read()
    ssh_authorize(username_to, key_local)
    key_file_local.close()

def authorize_key( username, keyfile ) :
    key_file = open(keyfile, "r")
    key = key_file.read();
    #puts(key)
    mode_sudo()
    ssh_authorize(username, key)
    key_file.close()

def authorize_user( username ) :
    # Read Keys Dir, and authorize key in remote host for user if key file name contains username
    keys_path = '/root/fabric/keys/'
    keys_files = os.listdir(keys_path)

    ## Authorize remote keys
    for key_filename in keys_files:
        if ( username in key_filename ):
            key_path = keys_path + key_filename
            key_file = open(key_path, "r")
            key = key_file.read()
            ssh_authorize(username, key)

            #with settings(host_string='127.0.0.1'):
            #    authorize_on_git(key_path)

            key_file.close()
    ## Authorize Local Key created in useradd
    #authorize_local_key( username, "git" )

def set_user_primary_group(group, user):
        """Adds the given user/list of users to the given group/groups."""
        assert group_check(group), "Group does not exist: %s" % (group)
        if not group_user_check(group, user):
                sudo("usermod -g '%s' '%s'" % (group, user))

#def deploy_nginx_vhost(username, dest) :

def userdel(username):
    if ( user_check(username)  ) :
        user_remove(username, True)
    else:
        puts("User Doesn't exists")

def create_src(username):
    location = "/home/%s/src/" % (username)
    sudo("mkdir %s" % (location))
    dir_attribs(location, mode=755, owner=username, group='developers', recursive=True)

def clone_git(username, repo, dest):
    #env.user = username
    env.user='centos'
    sudo("mkdir /home/%s/src/%s" % (username, dest))
    location = "/home/%s/src/%s" % (username, dest)
    #run( "git clone git@127.0.0.1:/home/git/%s %s" % (repo, location) )    	
    #run( "git clone git@bitbucket.org:pobreiluso/%s %s" % (repo, location) )    	
    run( "git clone git@bitbucket.org:chicfysl/%s %s" % (repo, location) )    	
    dir_attribs(location, mode=755, owner=username, group='developers', recursive=True)

def add_sudoers(username):
    run('echo "{user} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers'.format(user=username))

def deploy_olda(username, path):
    file_link('/var/www/static.chicfy.com/img/','/home/%s/src/%s/img' % (username, path))
    file_link('/var/www/static.chicfy.com/archivos/','/home/%s/src/%s/archivos' % (username, path))
    file_link('/var/www/static.chicfy.com/new_files/','/home/%s/src/%s/new_files' % (username, path))

#def deploy_rf( username ):
#   file_link('/media/synology/rf/data/img_data','/home/%s/src/img_data' % username)

@roles('git_server')
def authorize_on_git(key_path):
    key = open(key_path, "r")
    ssh_authorize('git', key.read())
